import os
import subprocess

import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import pysptk
from joblib import Parallel, delayed
from pyts.image import GramianAngularField
from tqdm import tqdm

def mfcc_features(audio, sampling_rate, normalise=False):
    mfcc = librosa.feature.mfcc(y=audio, n_mfcc=40, sr=sampling_rate)
    if normalise:
        mfcc_norm = np.mean(mfcc.T, axis=0)
        return mfcc_norm
    else:
        return mfcc


def mel_filters(audio, sampling_rate, normalise=False):
    mel_spec = librosa.feature.melspectrogram(y=audio, n_mels=40, sr=sampling_rate)
    if normalise:
        return np.mean(librosa.power_to_db(mel_spec, ref=np.max).T)
    else:
        return librosa.power_to_db(mel_spec, ref=np.max)
        #


def mel_filters_with_spectrogram(audio, sampling_rate, filename, normalise=False):
    plt.figure(figsize=(3, 2))
    logmel = librosa.feature.melspectrogram(y=audio, n_mels=40, sr=sampling_rate)
    logmel = librosa.power_to_db(logmel, ref=np.max)
    librosa.display.specshow(logmel)
    plt.savefig(filename)
    plt.close()


def gaf(audio):
    gasf = GramianAngularField(image_size=100, method='summation')
    X_gasf = gasf.fit_transform(audio.reshape((-1, len(audio))))
    _, x, y = X_gasf.shape
    return np.float32(X_gasf.reshape((x, y)))


def cut_audio(audio, sampling_rate, sample_size_in_seconds, overlap):
    """
    Method to split a audio signal into pieces based on `sample_size_in_seconds` and `overlap` parameters
    :param audio: The main audio signal to be split
    :param sampling_rate: The rate at which audio is sampled
    :param sample_size_in_seconds: number of seconds in each split
    :param overlap: in seconds, how much of overlap is required within splits
    :return: List of splits
    """
    if overlap >= sample_size_in_seconds:
        raise Exception("Please maintain this condition: sample_size_in_seconds > overlap")

    def add_to_audio_list(y):
        if len(y) / sampling_rate < sample_size_in_seconds:
            raise Exception(
                    f'Length of audio lesser than `sampling size in seconds` - {len(y) / sampling_rate} seconds, required {sample_size_in_seconds} seconds')
        y = y[:required_length]
        audio_list.append(y)

    audio_list = []
    required_length = sample_size_in_seconds * sampling_rate
    audio_in_seconds = len(audio) // sampling_rate

    # Check if the main audio file is larger than the required number of seconds
    if audio_in_seconds >= sample_size_in_seconds:
        start = 0
        end = sample_size_in_seconds
        left_out = None

        # Until highest multiple of sample_size_in_seconds is reached, ofcourse, wrt audio_in_seconds, run this loop
        while end <= audio_in_seconds:
            index_at_start, index_at_end = start * sampling_rate, end * sampling_rate
            one_audio_sample = audio[index_at_start:index_at_end]
            add_to_audio_list(one_audio_sample)
            left_out = audio_in_seconds - end
            start = (start - overlap) + sample_size_in_seconds
            end = (end - overlap) + sample_size_in_seconds

        # Whatever is left out after the iteration, just include that to the final list.
        # Eg: if 3 seconds is left out and sample_size_in_seconds is 5 seconds, then cut the last 5 seconds of the audio
        # and append to final list.
        if left_out > 0:
            one_audio_sample = audio[-sample_size_in_seconds * sampling_rate:]
            add_to_audio_list(one_audio_sample)
    # Else, just repeat the required number of seconds at the end. The repeated audio is taken from the start
    else:
        less_by = sample_size_in_seconds - audio_in_seconds
        excess_needed = less_by * sampling_rate
        one_audio_sample = np.append(audio, audio[-excess_needed:])

        # This condition is for samples which are too small and need to be repeated
        # multiple times to satisfy the `sample_size_in_seconds` parameter
        while len(one_audio_sample) < (sampling_rate * sample_size_in_seconds):
            one_audio_sample = np.hstack((one_audio_sample, one_audio_sample))
        add_to_audio_list(one_audio_sample)
    return audio_list

def read_audio_n_process(file, label, base_path, sampling_rate, sample_size_in_seconds, overlap, normalise, method):
    """
    This method is called by the preprocess data method
    :param file:
    :param label:
    :param base_path:
    :param sampling_rate:
    :param sample_size_in_seconds:
    :param overlap:
    :param normalise:
    :return:
    """
    data, out_labels = [], []
    filepath = base_path + file
    if os.path.exists(filepath):
        audio, sr = librosa.load(filepath, sr=sampling_rate)
        sr = sampling_rate
        chunks = cut_audio(audio, sampling_rate=sr, sample_size_in_seconds=sample_size_in_seconds,
                           overlap=overlap)
        for chunk in chunks:
            if method == 'fbank':
                zero_crossing = librosa.feature.zero_crossing_rate(chunk)
                f0 = pysptk.swipe(chunk.astype(np.float64), fs=sr, hopsize=510, min=60, max=240, otype="f0").reshape(1,
                                                                                                                     -1)
                pitch = pysptk.swipe(chunk.astype(np.float64), fs=sr, hopsize=510, min=60, max=240,
                                     otype="pitch").reshape(
                        1, -1)
                f0_pitch_multiplier = 1
                features = mel_filters(chunk, sr, normalise)
                f0 = np.reshape(f0[:, :features.shape[1] * f0_pitch_multiplier], newshape=(f0_pitch_multiplier, -1))
                pitch = np.reshape(pitch[:, :features.shape[1] * f0_pitch_multiplier],
                                   newshape=(f0_pitch_multiplier, -1))
                features = np.concatenate((features, zero_crossing, f0, pitch), axis=0)
            elif method == 'mfcc':
                features = mfcc_features(chunk, sr, normalise)
            elif method == 'gaf':
                features = gaf(chunk)
            elif method == 'raw':
                features = chunk
            else:
                raise Exception(
                        'Specify a method to use for pre processing raw audio signal. Available options - {fbank, mfcc, gaf, raw}')
            data.append(features)
            out_labels.append(float(label))
        return data, out_labels, chunks
    else:
        print('File not found ', filepath)
        return [], [], []


def preprocess_data(base_path, files, labels, normalise, sample_size_in_seconds, sampling_rate, overlap, method):
    data, out_labels, raw = [], [], []
    aggregated_data = Parallel(n_jobs=8, backend='multiprocessing')(
            delayed(read_audio_n_process)(file, label, base_path, sampling_rate, sample_size_in_seconds, overlap,
                                          normalise, method) for file, label in
            tqdm(zip(files, labels), total=len(labels)))

    for per_file_data in aggregated_data:
        # per_file_data[1] are labels for the audio file.
        # Might be an array as one audio file can be split into many pieces based on sample_size_in_seconds parameter
        for i, label in enumerate(per_file_data[1]):
            # per_file_data[0] is array of audio samples based on sample_size_in_seconds parameter
            data.append(per_file_data[0][i])
            raw.append(per_file_data[2][i])
            out_labels.append(label)
    return data, out_labels, raw