# AlcoAudio Research

Detection of Alcohol induced intoxication through voice using Neural Networks

## **Dataset**

[Alcohol Language Corpus](https://www.phonetik.uni-muenchen.de/Bas/BasALCeng.html) is a curation of audio samples from 162 speakers. Audio samples are first recorded when speaker is sober. Then the speakers are given a chosen amount of alcohol to reach a particular intoxication state, and audio samples are recorded again. 
## **Setup**

1. Download and run the requirements.txt to install all the dependencies.

      
       pip install -r requirements.txt -t ./
     
     
2. Create a [config](https://github.com/ShreeshaN/AlcoAudio/blob/master/alcoaudio/configs/shree_configs.json) file of your own

3. Install OpenSmile and set environment variable ```OPENSMILE_CONFIG_DIR``` to point to the config directory of OpenSmile installation.

## Usage

### **Data generation**

Run ```data_processor.py``` to generate data required for training the model. It reads the raw audio samples, splits into ```n``` seconds and generates Mel filters, also called as Filter Banks (```fbank``` paramater in config file. Other available audio features are ```mfcc``` & ```gaf```)

    PYTHONPATH=./ python3 -u ./alcoaudio/datagen/data_processor.py --configs_file ./alcoaudio/configs/configs.json

### **Training the network**

Using ```main.py``` one can train all the architectures mentioned in the above section.

    PYTHONPATH=./ python3 -u ./alcoaudio/main.py --train_net True --configs_file ./alcoaudio/configs/configs.json
